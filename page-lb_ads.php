<?php
/*
Template Name: Ads page from LocalBitcons.net
*/
?>

<?php

get_header();


function get_the_user_ip()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        //check ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        //to check ip is pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}


?>
<?php if (have_posts()) { ?>
    <!-- Блок заголовка  -->
    <header class="entry-header">
        <h1 class="entry-title">
            <?php the_title(); ?>
        </h1>
    </header>
    <!-- блок текста  -->
    <div class="entry-content">
        <p><?php the_content(); ?></p>
    </div>
<?php
} // end if
?>

<div id='id_user' value='<?php echo get_the_user_ip(); ?>'></div>
<div class="popup" id="popup1">
    <div class="popup-content">
        <div class="popup_form">
            <div class="popup_error">
                Извините но мы не можем обработать данный запрос
            </div>
        </div>
        <div class="popup_button">
            <button class="button" onclick="javascript:sendAjaxForm()">Отправить</button>
            <button class="button" onclick="javascript:PopUpHideOne()">Отмена</button>
        </div>
    </div>
</div>
<div class="popup" id="popup2">
    <div class="popup-content">
        <div class="popup_form">
            <div class="popup_error">
                Извините но мы не можем обработать данный запрос
            </div>
        </div>
        <div class="popup_button">
            <button class="button" onclick="javascript:PopUpHideTwo()">Отмена</button>
        </div>
    </div>
</div>

<!-- Start table -->
<div class="table">
    <div class="transaction" id="transaction_buy" value="Продать:">Продать:</div>
    <div class="transaction" id="transaction_sell" value="Купить:">Купить:</div>
</div>
<div class="table_lbc">
    <!-- Start column buy -->
    <div class="column">
        <div class="contain" id="contain_buy">
            <div class="loading">
                <div class="spinner"></div>
            </div>
        </div>
    </div>
    <!-- End column buy -->
    <!-- Start column sell -->
    <div class="column">
        <div class="contain" id="contain_sell">
            <div class="loading" id="loading">
                <div class="spinner"></div>
            </div>
        </div>
    </div>
    <!-- End column sell -->
</div>
<!-- End table -->

<script type="text/javascript" src="https://www.onlybestoffers.trade/wp-content/themes/freak/pop-message.js"></script>

<?php
get_footer();

?>