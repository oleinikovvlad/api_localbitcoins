window.onload = function () {
    request();
    
    $(".tile").click(createId);
};

var idTransaction;
var chekRequest = false;
var openPopup = false;

function openLoader() {
    var elems = document.querySelectorAll(".loading");
    for (var i = 0; i < elems.length; i++) {
        elems[i].style.display = "flex";
    }
}
function closeLoader() {
    var elems = document.querySelectorAll(".loading");
    for (var i = 0; i < elems.length; i++) {
        elems[i].style.display = "none";
    }
}
function request() {
    if (!chekRequest){
        openLoader();
        $.ajax({
            url: 'https://www.onlybestoffers.trade/wp-content/themes/freak/lb-ads/publication.php',
            type: "POST",
            success: function (response) {
                var test = $.parseJSON(response)
                $('#contain_sell').html(test['sell']);
                $('#contain_buy').html(test['buy']);
                closeLoader();
                if (openPopup){modal()};
                $(".tile").click(createId);
                setTimeout(request, 1000 * 60);
            },
            error: function (response) {
                $('#contain_sell').html('<div class="tile">Что-то пошло не так, попробуйте обновить страницу, или подождите 1 минуту. Спасибо, за понимание!</div>');
                $('#contain_buy').html('<div class="tile">Что-то пошло не так, попробуйте обновить страницу, или подождите 1 минуту. Спасибо, за понимание!</div>');
                setTimeout(request, 1000 * 60);
                closeLoader();
                modal();
            }
        });
    }
}

$(".transaction").click(function () {
    var id = this.id;
    if (id == 'transaction_buy') {
        if (document.getElementById("contain_buy").style.display == 'block') {
            document.getElementById("contain_buy").style.display = 'none';

        } else {
            document.getElementById("contain_buy").style.display = 'block';
            document.getElementById("contain_sell").style.display = 'none';
        }
    }
    if (id == 'transaction_sell') {
        if (document.getElementById("contain_sell").style.display == 'block') {
            document.getElementById("contain_sell").style.display = 'none';

        } else {
            document.getElementById("contain_sell").style.display = 'block';
            document.getElementById("contain_buy").style.display = 'none';
        }
    }
});

function createId (){
    idTransaction = this.id;
    modal();
}
function modal() {
    var txt = $('#' + idTransaction).text();
    txt = txt.split(" ");
    txt.forEach(function (item, i, arr) {
        if (arr[i] == 'Цена') {
            $price = (arr[i + 1]);
        }
        if (arr[i] == 'сделке:') {
            if (arr[i + 2] == '-') {
                $min = (arr[i + 1]);
                $max = (arr[i + 3]);
                $value = (arr[i + 4]);
            }
        }
    });

    if (!isNaN($price && $min && $max)) {
        if (idTransaction.includes("1_")) {
            html_code = "<span id='transaction' value='sell'>Сколько вы хотите продать?</span>";
        }
        if (idTransaction.includes("2_")) {
            html_code = "<span id='transaction' value='buy'>Сколько вы хотите купить?</span>";
        }
        html_code = html_code + constructForm();
        $('.popup_form').html('<form action="">' + html_code + '</form>');
    }

    PopUpShowOne();

    document.getElementById('amount_input').oninput = function () {
        var input = +  document.getElementById('amount_input').value;
        if (input >= $min) {
            if (input <= $max) {
                addClassTrue();
            } else {
                addClassError();
            }
        }
        else {
            addClassError();
        }
        input = input / $price;
        input = input.toFixed(6);
        document.getElementById('amount_BTC').value = input;

    }
    document.getElementById('amount_BTC').oninput = function () {
        var input = document.getElementById('amount_BTC').value;
        if (input >= ($min / $price)) {
            if (input <= ($max / $price)) {
                addClassTrue();
            } else {
                addClassError();
            }
        }
        else {
            addClassError();
        }
        input = input * $price;
        input = input.toFixed(2);
        document.getElementById('amount_input').value = input;
    }
}

function addClassTrue() {
    $("#amount_input").removeClass('error_input').html();
    $("#amount_input").addClass('true_input').html();
    $("#amount_BTC").removeClass('error_input').html();
    $("#amount_BTC").addClass('true_input').html();
};
function addClassError() {
    $("#amount_input").removeClass('true_input').html();
    $("#amount_input").addClass('error_input').html();
    $("#amount_BTC").removeClass('true_input').html();
    $("#amount_BTC").addClass('error_input').html();
};
function PopUpHideOne() {
    openPopup = false;
    $("#popup1").hide();
};
function PopUpHideTwo() {
    $("#popup2").hide();
};

function PopUpShowOne() {
    openPopup = true;
    $("#popup1").show();

};
function PopUpShowTwo() {
    $("#popup2").show();
};


function constructForm() {
    var html_code;
    html_code = "<span id='price' value='" + $price + "'>Цена: " + $price + " " + $value + "/BTC</span> ";
    html_code = html_code + "<div id='calculator'>";
    html_code = html_code + "<span id='amount' required>" + $value + " <input type='text' id='amount_input' placeholder='0.00' title='Ограничение: " + $min + "< и >" + $max + "'></span>";
    html_code = html_code + "<span id='BTC' required>BTC <input type='text' id='amount_BTC' placeholder='0.000000'></span>";
    html_code = html_code + "</div>";
    html_code = html_code + "<textarea id='textarea' rows='5' cols='34' name='text' placeholder='Оставте ваш коментарий'></textarea>";
    return html_code;
};

function constructAnswer(inputBTC, inputAmount) {
    var html_code;
    var transaction = document.getElementById('transaction').getAttribute('value')
    if (transaction.includes("sell")) {
        html_code = "<p>Вы отправили заявку на продажу:</p>";
    }
    if (transaction.includes("buy")) {
        html_code = "<p>Вы отправили заявку на покупку:</p>";
    }
    html_code = html_code + "<p>Курс: "+  + $price + " " + $value + "/BTC</p>" ;
    html_code = html_code + "<p>" + inputAmount + " " + $value + "</p> ";
    html_code = html_code + "<p>" + inputBTC + " BTC</p> <hr>";
    html_code = html_code + "<p> Нажмите кнопку чата для связи или дождитесь ответа. </p>";
    html_code = html_code + "<p> Если хотите подать новую заявку, обновите страницу. </p>";
    return html_code;
}

function sendAjaxForm() {
    var inputBTC = + document.getElementById('amount_BTC').value;
    var inputAmount = + document.getElementById('amount_input').value;
    var data = {
        action: 'send_email',
        price: $price,
        transaction: document.getElementById('transaction').getAttribute('value'),
        amount: inputAmount,
        BTC: inputBTC,
        id: document.getElementById('id_user').getAttribute('value'),
        message: document.getElementById('textarea').value,
        value: $value
    };

    if ((inputAmount >= $min) && (inputAmount <= $max)) {
        $.ajax({
            url: 'https://www.onlybestoffers.trade/wp-admin/admin-ajax.php',
            type: "POST",
            data: data,
            success: function (response) {
                PopUpHideOne();
                /* TODO */
                $('#contain_sell').html(constructAnswer(inputBTC, inputAmount));
                $('#contain_buy').html(constructAnswer(inputBTC, inputAmount));
                //$('.popup_form').html('<div class="true_input">' + constructAnswer(inputBTC, inputAmount) + '</div>');
                //PopUpShowTwo();
                chekRequest = true;
            },
            error: function (response) {
                PopUpHideOne();
                $('.popup_form').html('<div class="error_input>' + response + '</div>');
                PopUpShowTwo();
            }
        });
    } else {
        alert('Введите допустимые значения');
    }
};

