# API_LocalBitcoins

source code: github.com/Steambot33/LocalBitcoins-API-PHP.git

 1. Put the folder ('lb-ads') in the WordPress theme directory ;
 2. Paste your HMAC authentication key into the document: lb-ads/advertisements.php ;
 3. Use the string "include('lb-ads/advertisements.php')" to connect the output block in the WordPress theme code ;

 4. You can also use default page templates : page-lb_ads.php ;
	* Put the document in the WordPress theme directory ;
	* Use the template name ('Ads page from LocalBitcons.net') when creating a new page ;

 5. Edit the document (lb-ads/advertisements.php) replacing the styles with classes, and add the necessary styles to your css ;

 6. Done! Use with pleasure :) 